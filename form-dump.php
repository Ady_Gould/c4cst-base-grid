<?php
/**
 *  File:   form-dump.php
 *  Author: YOURNAME <YOUR.NAME@polytechnicwest.wa.edu.au>
 *  Date:   22/08/2016
 *  Version:
 *          1.0 Original form dump
 *              Simple PHP output of the form submission.
 */

/**
 *  The HTML in this file is taken from our forms.html file for
 *  simplicity, and time saving.
 */
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Form Submission Dumper</title>
        <link rel="stylesheet" href="../css/font-awesome.css">
        <link rel="stylesheet" href="../css/forms.css">
    </head>
    <body>
    <div class="wrapper">
        <header class="siteHeader">
            <h1>Form Submission Dumper</h1>
        </header>
        <div class="mainContent">
            <h2>Form Results</h2>

            <?php
            if (isset($_GET)) {
                $formResults["Get"] = $_GET;
            }

            if (isset($_POST)) {
                $formResults["Post"] = $_POST;
            }

            echo "</table>";

            foreach($formResults as $getPost => $formValues) {
                echo "<table>";
                echo "<thead>";
                echo "    <tr><th colspan='2' style='text-align:left;'>";
                echo "        <h3>Data sent via:{$getPost}</h3>";
                echo "    </th></tr>";
                echo "    <tr>";
                echo "        <th>Control Name/ID</th><th>Value Sent</th>";
                echo "    </tr>";
                echo "</thead>";
                echo "<tbody>";
                if (sizeof($formValues)<=0) {
                    echo "<tr><td>No Data Sent</td><td>-</td></tr>";
                } else {
                    foreach ($formValues as $formKey => $keyValue) {
                        echo "<tr><td>{$formKey}</td><td>{$keyValue}</td></tr>";
                    } //end for each
                } //end if
                echo "</tbody>";
            } // end for each
            echo "</table>";
            ?>
        </div>
        <!-- end main content -->

        <footer class="siteFooter">
            <p>&copy; Copyright YOUR NAME</p>
        </footer>
    </div>
    <!-- end wrapper -->
    </body>
    </html>
<?php /** always end a PHP file in PHP mode **/
